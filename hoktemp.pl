#! /usr/bin/perl -w

use strict;
use LWP::Simple;

# sensors
my %sensors = (
        'Rozijntje' => "https://rozijntje.snt.utwente.nl/temper/",
        'Kaakje' => "https://kaakje.snt.utwente.nl/temper/",
);

my $request = "all";

my $args = join(' ', @ARGV);
$args =~ s/\s+/ /g;
$args =~ s/\A\s//;
$args =~ s/\s\z//;
$args .= ' ';

my $kelvin_offset = 273.15;
my $do_kelvin = 0;
my $do_simple = 0;
my $do_colour = 0;

while ($args =~ s/\A--(\w+) //) {
    my $opt = lc $1;
    if ($opt eq 'kelvin') {
        $do_kelvin++;
    } elsif ($opt eq 'color') {
        $do_colour++;
    } elsif ($opt eq 'colour') {
        $do_colour++;
    } elsif ($opt eq 'simple') {
        $do_simple++;
    } else {
        print "Onbekende optie '$opt'.\n";
        exit 0;
    }
}

if ($args =~ /\S/) {
    $args =~ s/^\s+//;
    $args =~ s/\s+$//;
    $request = $args;
}

#TODO: add option to request more than one but not all.
my @requested_sensorts = grep { lc($request) eq lc($_) } keys %sensors;
if (@requested_sensorts == 0) {
    if (lc($request) eq 'all') {
        @requested_sensorts = keys %sensors
    } else {
        print "Sensor '$request' niet gevonden.\n";
        exit 0;
    }
}

my $temps = "";

foreach my $sensor (@requested_sensorts) {
    my $url = $sensors{$sensor};
    
    my $temp = get($url);
    
    if ($do_colour) {
        $temps
              .= $temp < 18 ? "\cC2,01"
              :  $temp < 25 ? "\cC3,01"
              :  $temp < 30 ? "\cC7,01"
              :  "\cC4,01";
    }
    
    $temp += $kelvin_offset if $do_kelvin;
    $temps .= $temp;
    $temps .= "\cC" if $do_colour;
    $temps .= "~";
}

chop $temps;
if ($do_kelvin) {
    $temps .= "K";
} else {
    $temps .= chr(176).'C';
}

my $out;

if ($do_simple) {
    $out = $temps;
} else {
    $out= "Momenteel is het $temps in het hok.";
}

print "$out\n";